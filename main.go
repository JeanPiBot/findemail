package main

import "fmt"

func main() {
	correos := [...]string{"correo1@hotmail.com", "correo2@hotmail.com", "correo3esde@gmail.com", "correodeprueba@hotmail.com", "correo5de@gmail.com", "correo6@gmail.com"}
	correosHotmail := []string{}

	for _, correo := range correos {
		for indice, value := range correo {
			if value == 64 {
				position := indice
				if correo[position:] == "@hotmail.com" {
					correosHotmail = append(correosHotmail, correo)
				}
				
			}
		}
	}

	fmt.Println("Los correos de Hotmail encontrados son", correosHotmail)
}